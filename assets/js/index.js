function createArticle(id, articles) {
    const article = articles.find(article => article.id == id);
    const articletemplate = `<div id="article">
    <div class="banner">
      <img class="object-fill h-1280 w-fit" src="${article.thumbnail}" alt="image">
    </div>
    <div class="title">
      <h1 class="text-3xl font-bold underline">${article.title}</h1>
    </div>
    <div class="written-by-author">
      <p class="text-base">Écrit par <a class="lien" href="#">${article.author}</a></p>
      <p class="text-base">Le ${article.createdAt}</p>
    </div>
    <div class="tags mb-3">
    </div>
    <div class="text-article">
      <p>
       ${article.content}
      </p>
    </div>
  </div>`;
    let tags = article.tags;
    document.querySelector('.content').innerHTML += articletemplate;
    tags.forEach(tag => {
        let tagtemplate = `<div class="bg-red-900 inline-flex items-center text-sm rounded mr-1 overflow-hidden">
        <span class="ml-2 mr-1 leading-relaxed truncate max-w-xs px-1" x-text="tag">${tag}</span>
      </div>`;
        document.querySelector('.tags').innerHTML += tagtemplate;
    });
}

function tagsToMeta(id) {
    const article = articles.find(article => article.id == id);
    let tag = [];
    for (var x = 0; x < article.tags.length; x++) {
        tag.push(article.tags[x].toLowerCase()); //tags en minuscule
    }
    tag = tag.join(',');
    let tagtemplate = `<meta name="keywords" content="${tag}">` // ajoute les tags dans les meta
    document.querySelector('head').innerHTML += tagtemplate;
    let description = article.content.substring(0, 160);
    description = description.replace(/(<([^>]+)>)/gi, ""); // enlève les balises html
    let descriptiontemplate = `<meta name="description" content="${description}">` // la description dans les meta
    document.querySelector('head').innerHTML += descriptiontemplate;
}

let articles = [{
        title: 'Les logiciels libres et propriétaires',
        thumbnail: 'https://th.bing.com/th/id/R.ce7f83f8ac25188a400341771a729908?rik=b%2fH3grr6ADJO%2bw&pid=ImgRaw&r=0',
        description: 'Dans cet article, nous allons voir les différences entre les logiciels libres et propriétaires. Nous verrons également les avantages et les inconvénients de chacun.',
        author: 'Noufele',
        id: 1,
        tags: ['Dev', 'Open-source', 'Propriétaire', 'Logiciel', 'Sécurité'],
        content: 'Un logiciel <b>open-source</b> est un logiciel dont le code source est accessible à tous. Il est donc possible de le modifier, de le redistribuer et de l\'améliorer. Ils sont souvent gratuits et sont aussi souvent développés par des communautés de développeurs indépendants. A contrario, les logiciels <b>propriétaires</b> sont des logiciels dont le source code n\'est pas disponible publiquement sur internet et est par extension souvent payant. Les logiciels open-source sont souvent plus sécurisés que les logiciels dit <b>propriétaires</b> car ils sont souvent vérifiés par des développeurs extérieurs. Les logiciels open-source sont souvent plus rapides que les logiciels propriétaires car ils sont souvent développés par des communautés de développeurs. Les logiciels open-source sont souvent plus rapides que les logiciels propriétaires car ils sont souvent développés par des communautés de développeurs. Aujourd\'hui les entreprises qui développent des logiciels qui ne sont pas open-source tendent quand même à laisser une partie de leur code open-source afin que d\'autres développeurs puissent contribuer en échange d\'une contribution monétaire. <h2 class="text-3xl font-bold" id="exemple-de-logiciel-libre">Exemple de logiciel libre</h2><ul class="list-disc"><li><a class="lien" href="https://www.mozilla.org/fr/firefox/new/">Firefox</a></li><li><a class="lien" href="https://fr.libreoffice.org/">LibreOffice</a></li><li><a class="lien" href="https://www.gimp.org/">Gimp</a></li><li><a class="lien" href="https://inkscape.org/fr/">Inkscape</a></li><li><a class="lien" href="https://www.blender.org/">Blender</a></li></ul><h2 class="text-3xl font-bold" id="exemple-de-logiciel-propri-taire">Exemple de logiciel propriétaire</h2><ul class="list-disc"><li><a class="lien" href="https://www.microsoft.com/fr-fr/microsoft-365/office">Microsoft Office</a></li><li><a class="lien" href="https://www.adobe.com/fr/products/photoshop.html">Adobe Photoshop</a></li><li><a class="lien" href="https://www.adobe.com/fr/products/illustrator.html">Adobe Illustrator</a></li></ul>',
        createdAt: '2022-09-20',
        link: 'assets/articles/article.html?id=1'
    },
    {
        title: 'Comment choisir la configuration d’un ordinateur ?',
        thumbnail: 'https://th.bing.com/th/id/R.6c905062a3171660d767cdfda286a9c4?rik=UWdfunVpKEDb8w&pid=ImgRaw&r=0',
        description: 'Pour choisir la configuration d\'un ordinateur, il faut prendre en compte plusieurs critères. Nous allons voir dans cet article les différents critères à prendre en compte pour choisir la configuration d\'un ordinateur.',
        author: 'Noufele',
        id: 2,
        tags: ['hardware', 'ordinateur', 'configuration', 'choisir', 'pc'],
        content: `
        <h3 class="text-3xl font-bold" id="le-processeur">Le processeur</h3><p>Le processeur ou CPU en anglais pour Central Processing Unit est le cerveau de l&#39;ordinateur. Il est composé de plusieurs unités de calculs appelées coeurs. Plus il y a de coeurs, plus l&#39;ordinateur est rapide. Il existe plusieurs types de processeurs : les processeurs Intel et AMD. Les processeurs Intel sont plus performants que les processeurs AMD. Les processeurs Intel sont plus performants que les processeurs AMD.</p><h3 class="text-3xl font-bold" id="la-carte-m-re">La carte mère</h3><p>La carte mère ou motherboard est la carte qui permet de connecter tous les composants de l&#39;ordinateur. Elle est composée de plusieurs ports et de plusieurs emplacements pour les composants. Elle est souvent équipée d&#39;un chipset qui permet de gérer les différents composants de l&#39;ordinateur. Elle est souvent équipée d&#39;un chipset qui permet de gérer les différents composants de l&#39;ordinateur.</p><h3 class="text-3xl font-bold" id="la-memoire-vive">La mémoire vive</h3><p>La mémoire vive ou RAM est la mémoire vive de l&#39;ordinateur. L&#39;ordinateur utilise la mémoire vive pour stocker les programmes et les données en cours d&#39;utilisation. Plus il y a de mémoire vive, plus l&#39;ordinateur est rapide.</p><h3 class="text-3xl font-bold" id="le-disque-dur">Le disque dur</h3><p>Le disque dur est le disque dur de l&#39;ordinateur. Il est composé de plusieurs disques durs. Plus il y a de disques durs, plus l&#39;ordinateur est rapide. Il existe plusieurs types de disques durs : les disques durs SSD et HDD. Les disques durs SSD sont plus performants que les disques durs HDD. Les disques durs SSD sont plus performants que les disques durs HDD.</p><h3 class="text-3xl font-bold" id="la-carte-graphique">La carte graphique</h3><p>La carte graphique est la carte graphique de l&#39;ordinateur. Elle est composée de plusieurs cartes graphiques. Plus il y a de cartes graphiques, plus l&#39;ordinateur est rapide. Il existe plusieurs types de cartes graphiques : les cartes graphiques Intel et AMD. Les cartes graphiques Intel sont plus performantes que les cartes graphiques AMD. Les cartes graphiques Intel sont plus performantes que les cartes graphiques AMD. Elle est cependant optionnelle car la plupart des ordinateurs sont équipés d&#39;une carte graphique intégrée au processeur mais devient indispensable pour des tâches graphiques lourdes.</p><h2 class="text-3xl font-bold" id="exemple-de-configurations-d-un-ordinateur">Exemple de configurations d’un ordinateur</h2><p>Pour bien choisir <strong>la configuration d&#39;un ordinateur</strong>, il faut d&#39;abord définir l&#39;utilisation que l&#39;on souhaite en faire. En effet, il existe des ordinateurs pour les jeux, pour le travail, pour la bureautique, pour la vidéo, etc. Dépendant de ce que l&#39;on souhaite faire, il faudra choisir un ordinateur avec une configuration adaptée : on sait qu&#39;un ordinateur se compose d&#39;un processeur, qui va être comme un cerveau pour l&#39;ordinateur, d&#39;une carte mère qui va être comme le cœur de l&#39;ordinateur, de la mémoire vive qui va être comme la mémoire de l&#39;ordinateur, d&#39;un disque dur qui va être comme le stockage de l&#39;ordinateur, d&#39;une carte graphique qui va être comme l&#39;œil de l&#39;ordinateur. <br>Par exemple, pour jouer, il faudra choisir <strong>un ordinateur avec une carte graphique puissante, un processeur puissant et une bonne quantité de mémoire vive</strong>. Pour travailler, <strong>il faudra choisir un ordinateur avec un processeur puissant, une bonne quantité de mémoire vive et un disque dur rapide. Pour la bureautique, il faudra choisir un ordinateur avec un processeur puissant, une bonne quantité de mémoire vive et un disque dur rapide</strong>. Pour la vidéo, <strong>il faudra choisir un ordinateur avec une carte graphique puissante, un processeur puissant et une bonne quantité de mémoire vive</strong>.</p><p>Enfin de compte, il faut savoir que l&#39;on peut toujours changer la configuration d&#39;un ordinateur en achetant des composants supplémentaires si celle-ci nous le propose et si on a les moyens de le faire. </p>`,
        createdAt: '2022-09-27',
        link: 'assets/articles/article.html?id=2'
    },
    
    {
        title: 'Bien choisir ses contrats de maintenance',
        thumbnail: 'https://3ja7kv3vb64k3jba19afwdp1-wpengine.netdna-ssl.com/wp-content/uploads/2018/03/ContractEvent.jpg',
        description: 'Choisir ses contrats de maintenance est une étape importante dans la vie d\'une entreprise. Nous allons voir dans cet article les différents types de contrats de maintenance et les critères à prendre en compte pour choisir le contrat de maintenance le plus adapté.',
        author: 'Noufele',
        id: 3,
        tags: ['contrat', 'maintenance'],
        content: `<p>Le choix de ses <strong>contrats de maintenance</strong> est très importante pour un flux de travail optimal. En effet, il est important de bien choisir ses contrats de maintenance afin de ne pas avoir de perte de données, de perte de temps et de perte d&#39;argent. Ces contrats de maintenance sont très importants car ils permettent de garantir un service de qualité. </p><h2 class="text-3xl font-bold" id="les-diff-rents-types-de-contrats-de-maintenance">Les différents types de contrats de maintenance</h2><p>Il existe plusieurs types de <strong>contrats de maintenance informatique</strong>. <strong>Le contrat de maintenance préventive</strong>: il permet de prévenir les pannes et les dysfonctionnements de l&#39;ordinateur. Il permet de faire des vérifications régulières afin de détecter les problèmes avant qu&#39;ils ne surviennent. <strong>Le contrat de maintenance corrective</strong>: il permet de réparer les pannes et les dysfonctionnements de l&#39;ordinateur. Il permet de réparer les problèmes qui surviennent. Le contrat de maintenance préventive et corrective: il permet de prévenir les pannes et les dysfonctionnements de l&#39;ordinateur et de les réparer. Il permet de faire des vérifications régulières afin de détecter les problèmes avant qu&#39;ils ne surviennent et de les réparer.</p><h2 class="text-3xl font-bold" id="comment-choisir-son-op-rateur-de-maintenance-informatique-">Comment choisir son opérateur de maintenance informatique ?</h2><p>Pour bien choisir son opérateur de maintenance informatique, il faut d&#39;abord définir ses besoins. En effet, il existe des opérateurs de maintenance informatique qui proposent des contrats de maintenance préventive, des contrats de maintenance corrective et des contrats de maintenance préventive et corrective. Il faut donc définir ses besoins afin de choisir l&#39;opérateur de maintenance informatique qui propose le contrat de maintenance qui correspond à ses besoins. <br>Ensuite, il faut définir son budget : Il existe des opérateurs de maintenance informatique qui proposent des services de qualité à des prix très élevés et il existe des opérateurs de maintenance informatique qui proposent des services de qualité à des prix très bas. Il faut donc définir son budget afin de choisir l&#39;opérateur de maintenance informatique qui propose le contrat de maintenance qui correspond à son budget et son degré de criticité. Par exemple, si on a juste besoin d&#39;une action qui ne sera pas critique, on peut se dire que l&#39;on peut prendre un contrat de maintenance à un prix bas mais il faut tout de même faire attention car il existe des opérateurs de maintenance informatique qui proposent des services de qualité à des prix très bas mais qui ne sont pas fiables.<br>Enfin, le taux de disponibilité est très important : il faut savoir que le taux de disponibilité est très important car il permet de savoir si l&#39;opérateur de maintenance informatique est fiable ou non. En effet, si l&#39;opérateur de maintenance informatique a un taux de disponibilité très bas, cela veut dire qu&#39;il y a beaucoup de problèmes avec l&#39;opérateur de maintenance informatique et que l&#39;on ne peut pas lui faire confiance. Mais généralement, les opérateurs de maintenance informatique ont un taux de disponibilité très élevé, maintenant est-ce que c&#39;est fiable ou non, c&#39;est une autre histoire.</p><p>Il est donc primordiale de bien choisir son opérateur de maintenance informatique afin de ne pas avoir ce genre de problèmes. </p>`,
        createdAt: '2022-10-11',
        link: 'assets/articles/article.html?id=3'
    },
    {
        title: 'Héberger son site internet',
        thumbnail: 'https://th.bing.com/th/id/OIP.2wKwkoeZms-5bE0CnU-dbQHaCj?pid=ImgDet&rs=1',
        description: 'Héberger son site internet est une étape importante dans la vie d\'une entreprise. Nous allons voir dans cet article les différents moyens d\'héberger son site web et les critères à prendre en compte pour choisir celui le plus adapté.',
        author: 'Noufele',
        id: 4,
        tags: ['hébergement', 'site', 'internet', 'web', 'serveur', 'cloud'],
        content: '<p>Lorsque l&#39;on a un site internet la finalité pour celui-ci est de le rendre accessible à tous. Pour cela, il faut héberger son site internet. Il existe plusieurs types d&#39;hébergement. L&#39;hébergement mutualisé est le type d&#39;hébergement le plus simple et le plus utilisé pour les sites internet avec peu de traffic. Il permet de partager un serveur avec d&#39;autres personnes sur un même serveur. L&#39;hébergement dédié est le type d&#39;hébergement le plus puissant cependant il est aussi le plus cher, il permet d&#39;avoir un serveur dédié à l&#39;hébergement de son site internet. Sur le marché, il existe plusieurs entreprises qui proposent des solutions d&#39;hébergement comme OVH, Gandi, etc. </p><p>Il est important de bien choisir son hébergeur car il est possible que celui-ci soit en panne ou qu&#39;il y ait des problèmes de sécurité. Il est donc important de bien choisir son hébergeur car il est possible que celui-ci soit en panne ou qu&#39;il y ait des problèmes de sécurité. <a class="lien" href="#bien-choisir-ses-contrats-de-maintenance-informatique">on vous renvoie à notre article sur les contrats de maintenance informatique</a>. </p><p>Parle de cloudflare et de la mise en cache des pages web. : <a class="lien" href="https://www.cloudflare.com/fr-fr/learning/cdn/what-is-a-cdn/">https://www.cloudflare.com/fr-fr/learning/cdn/what-is-a-cdn/</a></p><p>Cloudflare est un service qui permet notamment d&#39;éviter à votre site web de tomber en panne en mettant en cache les pages web de votre site internet et de le protéger contre les attaques DDoS. </p><h1 id="comment-choisir-son-hebergeur-"><b>Comment choisir son hébergeur ?</b></h1><p>Pour choisir son hébergeur, il faut d&#39;abord définir ses besoins. En effet, il existe des hébergeurs qui proposent des services de qualité à des prix très élevés et il existe des hébergeurs qui proposent des services de qualité à des prix très bas. Il faut donc définir ses besoins afin de choisir l&#39;hébergeur qui propose le service qui correspond à ses besoins. Par exemple, il existe chez OVH plusieurs tier de services d&#39;hébergement : en premier lieu on peut avoir les serveurs partagés qu&#39;on appelle aussi VPS où l&#39;on va souvent parler d&#39;hébergement mutualisé, ensuite on peut avoir les serveurs dédiés et enfin on peut avoir les serveurs dédiés cloud. Il faut donc définir ses besoins afin de choisir l&#39;hébergeur qui propose le service qui correspond à ses besoins.</p><p>Pour le budget il faut définir son budget afin de choisir l&#39;hébergeur qui propose le service qui correspond à son budget. Par exemple, si on a juste besoin d&#39;un hébergement mutualisé, on peut se dire que l&#39;on peut prendre un hébergement à un prix bas. Encore une fois, tout dépend de vos besoins. Vous pouvez voir les différents types d&#39;hébergement chez OVH ici : <a class="lien" href="https://www.ovh.com/fr/hebergement-web/">https://www.ovh.com/fr/hebergement-web/</a></p><p><img class="article-image" src="../images/grille_ovh.png" alt="Prix hébergement OVH"></p><h2 class="text-3xl font-bold" id="plan-de-sauvegarde"><b>Plan de sauvegarde</b></h2><p>Il faut aussi penser à faire un plan de sauvegarde. En effet, il est important de faire un plan de sauvegarde car il est possible que votre site internet soit piraté ou qu&#39;il y ait un problème avec votre hébergeur. On se rappelle de l&#39;incident OVH en 2021 où un incendie a eu lieu dans un datacenter d&#39;OVH et qui a fait tomber en panne tous les serveurs d&#39;OVH et qui pour ceux qui n&#39;avaient pas de plan de sauvegarde ont perdu tous leurs fichiers. Il est donc important de faire un plan de sauvegarde pour éviter de perdre tous ses fichiers. On vous renvoi vers cette article qui explique plus en détails <a class="lien" href="https://www.journaldunet.com/web-tech/cloud/1498567-incendie-d-ovh-une-action-collective-lancee-par-sept-entreprise/" target="_blank">l&#39;incident OVH.</a></p><p><a class="lien" href="https://www.ovh.com/fr/hebergement-web/">Source 1 </a><a class="lien" href="https://www.gandi.net/fr/hebergement">Source 2</a></p>',
        createdAt: '2022-10-11',
        link: 'assets/articles/article.html?id=4'
    },
    {
        title: 'La communication sur un réseau',
        thumbnail: 'https://th.bing.com/th/id/R.ff45bc9b6f6fd48d1e51ba9a3aaa0cd7?rik=pWzQwdoVt688Rw&pid=ImgRaw&r=0',
        description: 'Pour communiquer sur un réseau, il existe plusieurs protocoles. Nous allons voir dans cet article les différents protocoles de communication sur un réseau et les critères à prendre en compte pour choisir le protocole de communication le plus adapté.',
        author: 'Noufele',
        id: 5,
        tags: ['tcp', 'udp', 'protocole', 'communication', 'réseau', 'internet'],
        content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt quisquam voluptate totam delectus, ut asperiores, alias libero eveniet quod quasi architecto officia blanditiis deserunt voluptatum eos? Et, blanditiis totam. Beatae!',
        createdAt: '2022-10-11',
        link: 'assets/articles/article.html?id=5'
    },
    {
        title: 'Les licences logiciels',
        thumbnail: 'https://chronicler.tech/content/images/2019/07/blog-image-licensing.jpg',
        description: 'à la fin de la conception d\'un logiciel, il faut choisir la licence qui sera appliquée à ce logiciel. Nous allons voir dans cet article les différents types de licences logiciels et les critères à prendre en compte pour choisir la licence logiciel la plus adaptée.',
        author: 'Noufele',
        id: 6,
        tags: ['licence', 'logiciel', 'logiciel', 'propriétaire', 'libre', 'open source'],
        content: '<p>On trouve différentes types de licences disponible, on en recense plus d&#39;une vingtaine mais on les divise généralement par quatre : </p><ul><li><b>Licences propriétaires</b> : l’acquéreur d’un logiciel propriétaire doit accepter un contrat de licence utilisateur final. Le code source est fermé. 1 poste=1 licence. Licence payante.</li><li><b>Licences freewares</b> : logiciel au code source fermé distribué gratuitement.</li><li><b>Licences sharewares</b> : Logiciel propriétaire que l’on se procure gratuitement pour le tester mais que l’on doit payer si l’on désire l’utiliser. Le code source est fermé.</li><li><b>Licences libres</b> : Logiciel à code source ouvert modifiable et redistribuable facilement par tout contributeur. Il existe de nombreuses variantes de licences libres encadrant plus ou moins le droit d’auteur. Il existe de nombreux flous juridiques aujourd’hui parmi les licences libres.</li></ul><h2 class="text-3xl font-bold" id="exemple-de-licences-libres">Exemple de licences libres</h2><p>On peut notamment nommer les licences libres suivantes :</p><ul><li>GPL : GNU General Public License</li><li>LGPL : GNU Lesser General Public License</li><li>BSD : Berkeley Software Distribution</li><li>MIT : Massachusetts Institute of Technology</li><li>Apache : Apache License</li></ul><p>Toutes ces licences libres sont compatibles entre elles et peuvent être utilisées conjointement dans un même projet.</p>',
        createdAt: '2022-10-11',
        link: 'assets/articles/article.html?id=6'
    },
    {
        title: 'Où trouve t-on les informaticiens ?',
        thumbnail: 'https://www.hbm.com/fileadmin/mediapool/images/people/contact-service-support/service-support.png',
        description: 'Les informaticiens sont partout. Nous allons voir dans cet article les différents secteurs d\'activité dans lesquels on trouve les informaticiens.',
        author: 'Noufele',
        id: 7,
        tags: ['métier', 'informatique', 'secteur', 'activité', 'entreprise', 'service'],
        content: `Aujourd'hui on peut trouver des informaticiens dans n'importe quel domaine de travaux : dans les entreprises, dans les administrations, dans les écoles, dans les universités, dans les hôpitaux, dans les banques, etc. On peut également trouver des informaticiens dans les entreprises de services informatiques, ils ne sont pas exempter. La mission des informaticiens est de gérer les systèmes informatiques, les réseaux informatiques, les bases de données, les serveurs, les postes de travail, etc. Pour être un bon informaticien selon moi il faut être passionné par l'informatique, avoir de la culture générale,en réseau,en sécurité informatique, en développement informatique, en administration système,   en administration réseau,en administration base de données,en administration serveur. `,
        createdAt: '2022-10-11',
        link: 'assets/articles/article.html?id=7'
    },
    {
        title: 'Pourquoi faire de la gestion de projet dans un service informatique ?',
        thumbnail: 'https://static.vecteezy.com/system/resources/previews/000/622/840/original/to-do-list-page-with-check-marks-and-pencil-concept-illustration-for-time-and-project-management-vector-illustration-template-in-flat-style.jpg',
        description: 'La gestion de projet pour un service informatique est devenu primordiale. Nous allons voir dans cet article les différents avantages de la gestion de projet dans un service informatique.',
        author: 'Noufele',
        id: 8,
        tags: ['gestion', 'projet', 'service', 'informatique', 'avantage', 'entreprise'],
        content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt quisquam voluptate totam delectus, ut asperiores, alias libero eveniet quod quasi architecto officia blanditiis deserunt voluptatum eos? Et, blanditiis totam. Beatae!',
        createdAt: '2022-10-11',
        link: 'assets/articles/article.html?id=8'
    },
    {
        title: 'Endpoint security',
        thumbnail: 'https://media.istockphoto.com/vectors/hacker-man-broken-lock-security-by-hand-cyber-crime-concept-vector-id1097676716',
        description: 'You have a workstation and you want to secure it. We will see in this article the different ways to secure a workstation.',
        author: 'Noufele',
        id: 9,
        tags: ['security', 'workplace', 'antivirus', 'firewall'],
        content: 'At a time where every computer that is connected to a network, the security of the device became huge critical field that is essential for companies. A computer that is not secured can be infected by some kind of program that will collect informations that aren’t supposed to be public or even spread out through the network to other computers on the network. <br> <br> To secure a computer, there are some things that you have to do : <br> <br> <ul> <li>Use a strong password</li> <li>Use an antivirus</li> <li>Use a firewall</li> <li>Use a software that encrypts your data in case of theft</li> <li>Use a software that backs up your data</li> </ul>',
        createdAt: '2022-10-11',
        link: 'assets/articles/article.html?id=9'
    },
    {
        title:'Comment faire une recherche efficace sur un internet ? ',
        thumbnail:'https://francoischarron.com/datascontent/10-trucs-recherches-precises-pertinentes-google-moteur-de-recherche.jpg',
        description:'Nous allons voir dans cet article comment faire une bonne recherche sur internet.',
        author:'Noufele',
        id:10,
        tags:['recherche','internet','google','yahoo','bing','moteur','recherche'],
        content:'',
        createdAt:'2022-10-11',
        link:'assets/articles/article.html?id=10'}

];


if (window.location.href.includes('article.html')) {
    let id = window.location.href.split('id=')[1];
    if (id == undefined) {
        id = 1;
    }
    try {
        createArticle(id, articles);
        tagsToMeta(id);
    } catch (e) {
        console.log(e);
        document.querySelector('.content').innerHTML = 'Article introuvable';
    }
} else if (window.location.href.includes('index.html') || window.location.pathname == '/') {
    let last_articles = document.querySelector("#last-articles > div:nth-child(2)");
    //let trending = document.querySelector('#trending');
    articles.forEach(article => {
        const cardtemplate = `
    <div class="my-8 rounded shadow-lg shadow-gray-200 dark:shadow-gray-900 bg-white dark:bg-gray-800 duration-300 hover:-translate-y-1">
        <img class="rounded-t h-72 w-full object-fill" src="${article.thumbnail}" alt="thumbnail">
        <h3 class="text-lg pd-10 font-bold leading-relaxed text-gray-800 dark:text-gray-300">${article.title}</h3>
        <p class="leading-5 text-gray-500 dark:text-gray-400">${article.description}</p>
        <div id="written-by-author">
            <p class="text-lg text-gray-500">Écrit par ${article.author}</p>
        </div>
        <a class="lien" href="${article.link}" class="text-blue-500 hover:text-sky-400">Lire l'article</a>
    </div>`;
        last_articles.innerHTML += cardtemplate;
    });
}

// listen to the click event on the button
document.querySelector('.rick').addEventListener('click', function () {
    alert('Désolé');
});